﻿namespace Asterisk
{
    partial class Asterisk_Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asterisk_Dashboard));
            this.s_edit_btn = new System.Windows.Forms.TabControl();
            this.dashboard_tab = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.stud_tab = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.s_add_btn = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.s_delete_btn = new System.Windows.Forms.Button();
            this.s_fetch_btn = new System.Windows.Forms.Button();
            this.s_dgv = new System.Windows.Forms.DataGridView();
            this.emp_tab = new System.Windows.Forms.TabPage();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.e_add_btn = new System.Windows.Forms.Button();
            this.e_edit_btn = new System.Windows.Forms.Button();
            this.e_delete_btn = new System.Windows.Forms.Button();
            this.e_fetch_btn = new System.Windows.Forms.Button();
            this.e_dgv = new System.Windows.Forms.DataGridView();
            this.emp_cont_tab = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.ec_add_btn = new System.Windows.Forms.Button();
            this.ec_edit_btn = new System.Windows.Forms.Button();
            this.ec_delete_btn = new System.Windows.Forms.Button();
            this.ec_fetch_btn = new System.Windows.Forms.Button();
            this.ec_dgv = new System.Windows.Forms.DataGridView();
            this.Roles_tab = new System.Windows.Forms.TabPage();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.r_add_btn = new System.Windows.Forms.Button();
            this.r_edit_btn = new System.Windows.Forms.Button();
            this.r_delete_btn = new System.Windows.Forms.Button();
            this.r_fetch_btn = new System.Windows.Forms.Button();
            this.r_dgv = new System.Windows.Forms.DataGridView();
            this.vst_tab = new System.Windows.Forms.TabPage();
            this.vt_add_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.vt_edit_btn = new System.Windows.Forms.Button();
            this.vt_delete_btn = new System.Windows.Forms.Button();
            this.vt_fetch_btn = new System.Windows.Forms.Button();
            this.vt_dgv = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.File_Toolbar = new System.Windows.Forms.ToolStripDropDownButton();
            this.File_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.User_toolbar = new System.Windows.Forms.ToolStripDropDownButton();
            this.Change_user_tool = new System.Windows.Forms.ToolStripMenuItem();
            this.Create_New_user_tool = new System.Windows.Forms.ToolStripMenuItem();
            this.manageUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentRefresh = new System.Windows.Forms.Timer(this.components);
            this.EmployerRefresh = new System.Windows.Forms.Timer(this.components);
            this.ContactsRefresh = new System.Windows.Forms.Timer(this.components);
            this.RolesRefresh = new System.Windows.Forms.Timer(this.components);
            this.VisitingRefresh = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.s_edit_btn.SuspendLayout();
            this.dashboard_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.stud_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.s_dgv)).BeginInit();
            this.emp_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.e_dgv)).BeginInit();
            this.emp_cont_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ec_dgv)).BeginInit();
            this.Roles_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.r_dgv)).BeginInit();
            this.vst_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vt_dgv)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // s_edit_btn
            // 
            this.s_edit_btn.Controls.Add(this.dashboard_tab);
            this.s_edit_btn.Controls.Add(this.stud_tab);
            this.s_edit_btn.Controls.Add(this.emp_tab);
            this.s_edit_btn.Controls.Add(this.emp_cont_tab);
            this.s_edit_btn.Controls.Add(this.Roles_tab);
            this.s_edit_btn.Controls.Add(this.vst_tab);
            this.s_edit_btn.Location = new System.Drawing.Point(12, 28);
            this.s_edit_btn.Name = "s_edit_btn";
            this.s_edit_btn.SelectedIndex = 0;
            this.s_edit_btn.Size = new System.Drawing.Size(1136, 600);
            this.s_edit_btn.TabIndex = 1;
            // 
            // dashboard_tab
            // 
            this.dashboard_tab.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dashboard_tab.Controls.Add(this.pictureBox1);
            this.dashboard_tab.Location = new System.Drawing.Point(4, 22);
            this.dashboard_tab.Name = "dashboard_tab";
            this.dashboard_tab.Padding = new System.Windows.Forms.Padding(3);
            this.dashboard_tab.Size = new System.Drawing.Size(1128, 574);
            this.dashboard_tab.TabIndex = 0;
            this.dashboard_tab.Text = "Dashboard";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 60);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // stud_tab
            // 
            this.stud_tab.Controls.Add(this.textBox1);
            this.stud_tab.Controls.Add(this.comboBox1);
            this.stud_tab.Controls.Add(this.s_add_btn);
            this.stud_tab.Controls.Add(this.button16);
            this.stud_tab.Controls.Add(this.s_delete_btn);
            this.stud_tab.Controls.Add(this.s_fetch_btn);
            this.stud_tab.Controls.Add(this.s_dgv);
            this.stud_tab.Location = new System.Drawing.Point(4, 22);
            this.stud_tab.Name = "stud_tab";
            this.stud_tab.Size = new System.Drawing.Size(1128, 574);
            this.stud_tab.TabIndex = 2;
            this.stud_tab.Text = "Student";
            this.stud_tab.UseVisualStyleBackColor = true;
            this.stud_tab.Enter += new System.EventHandler(this.student_refresh);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(155, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(161, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Student ID",
            "Forename",
            "Surname",
            "Faculty",
            "Degree",
            "Email",
            "Mobile"});
            this.comboBox1.Location = new System.Drawing.Point(6, 31);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(143, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // s_add_btn
            // 
            this.s_add_btn.Location = new System.Drawing.Point(885, 546);
            this.s_add_btn.Name = "s_add_btn";
            this.s_add_btn.Size = new System.Drawing.Size(75, 23);
            this.s_add_btn.TabIndex = 4;
            this.s_add_btn.Text = "Add";
            this.s_add_btn.UseVisualStyleBackColor = true;
            this.s_add_btn.Click += new System.EventHandler(this.s_add_btn_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(966, 546);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 5;
            this.button16.Text = "View/Edit";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // s_delete_btn
            // 
            this.s_delete_btn.Location = new System.Drawing.Point(1047, 546);
            this.s_delete_btn.Name = "s_delete_btn";
            this.s_delete_btn.Size = new System.Drawing.Size(75, 23);
            this.s_delete_btn.TabIndex = 6;
            this.s_delete_btn.Text = "Delete";
            this.s_delete_btn.UseVisualStyleBackColor = true;
            this.s_delete_btn.Click += new System.EventHandler(this.s_delete_btn_Click);
            // 
            // s_fetch_btn
            // 
            this.s_fetch_btn.Location = new System.Drawing.Point(6, 3);
            this.s_fetch_btn.Name = "s_fetch_btn";
            this.s_fetch_btn.Size = new System.Drawing.Size(75, 23);
            this.s_fetch_btn.TabIndex = 0;
            this.s_fetch_btn.Text = "Fetch Data";
            this.s_fetch_btn.UseVisualStyleBackColor = true;
            this.s_fetch_btn.Click += new System.EventHandler(this.s_fetch_btn_Click);
            // 
            // s_dgv
            // 
            this.s_dgv.AllowUserToAddRows = false;
            this.s_dgv.AllowUserToDeleteRows = false;
            this.s_dgv.AllowUserToResizeRows = false;
            this.s_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.s_dgv.Location = new System.Drawing.Point(6, 58);
            this.s_dgv.MultiSelect = false;
            this.s_dgv.Name = "s_dgv";
            this.s_dgv.ReadOnly = true;
            this.s_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.s_dgv.Size = new System.Drawing.Size(1116, 484);
            this.s_dgv.TabIndex = 3;
            this.s_dgv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Edit_stud);
            // 
            // emp_tab
            // 
            this.emp_tab.Controls.Add(this.textBox2);
            this.emp_tab.Controls.Add(this.comboBox2);
            this.emp_tab.Controls.Add(this.e_add_btn);
            this.emp_tab.Controls.Add(this.e_edit_btn);
            this.emp_tab.Controls.Add(this.e_delete_btn);
            this.emp_tab.Controls.Add(this.e_fetch_btn);
            this.emp_tab.Controls.Add(this.e_dgv);
            this.emp_tab.Location = new System.Drawing.Point(4, 22);
            this.emp_tab.Name = "emp_tab";
            this.emp_tab.Size = new System.Drawing.Size(1128, 574);
            this.emp_tab.TabIndex = 3;
            this.emp_tab.Text = "Employers";
            this.emp_tab.UseVisualStyleBackColor = true;
            this.emp_tab.Enter += new System.EventHandler(this.Employer_Refresh);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(155, 31);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(161, 20);
            this.textBox2.TabIndex = 2;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownWidth = 143;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Employer ID",
            "Employer",
            "Address 1",
            "Address 2",
            "Settlement",
            "County",
            "Post Code",
            "Country"});
            this.comboBox2.Location = new System.Drawing.Point(6, 31);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(143, 21);
            this.comboBox2.TabIndex = 1;
            // 
            // e_add_btn
            // 
            this.e_add_btn.Location = new System.Drawing.Point(885, 546);
            this.e_add_btn.Name = "e_add_btn";
            this.e_add_btn.Size = new System.Drawing.Size(75, 23);
            this.e_add_btn.TabIndex = 4;
            this.e_add_btn.Text = "Add";
            this.e_add_btn.UseVisualStyleBackColor = true;
            this.e_add_btn.Click += new System.EventHandler(this.e_add_btn_Click);
            // 
            // e_edit_btn
            // 
            this.e_edit_btn.Location = new System.Drawing.Point(966, 546);
            this.e_edit_btn.Name = "e_edit_btn";
            this.e_edit_btn.Size = new System.Drawing.Size(75, 23);
            this.e_edit_btn.TabIndex = 5;
            this.e_edit_btn.Text = "View/Edit";
            this.e_edit_btn.UseVisualStyleBackColor = true;
            this.e_edit_btn.Click += new System.EventHandler(this.e_edit_btn_Click);
            // 
            // e_delete_btn
            // 
            this.e_delete_btn.Location = new System.Drawing.Point(1047, 546);
            this.e_delete_btn.Name = "e_delete_btn";
            this.e_delete_btn.Size = new System.Drawing.Size(75, 23);
            this.e_delete_btn.TabIndex = 6;
            this.e_delete_btn.Text = "Delete";
            this.e_delete_btn.UseVisualStyleBackColor = true;
            this.e_delete_btn.Click += new System.EventHandler(this.e_delete_btn_Click);
            // 
            // e_fetch_btn
            // 
            this.e_fetch_btn.Location = new System.Drawing.Point(6, 3);
            this.e_fetch_btn.Name = "e_fetch_btn";
            this.e_fetch_btn.Size = new System.Drawing.Size(75, 23);
            this.e_fetch_btn.TabIndex = 0;
            this.e_fetch_btn.Text = "Fetch Data";
            this.e_fetch_btn.UseVisualStyleBackColor = true;
            this.e_fetch_btn.Click += new System.EventHandler(this.e_fetch_btn_Click);
            // 
            // e_dgv
            // 
            this.e_dgv.AllowUserToAddRows = false;
            this.e_dgv.AllowUserToDeleteRows = false;
            this.e_dgv.AllowUserToResizeRows = false;
            this.e_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.e_dgv.Location = new System.Drawing.Point(6, 58);
            this.e_dgv.MultiSelect = false;
            this.e_dgv.Name = "e_dgv";
            this.e_dgv.ReadOnly = true;
            this.e_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.e_dgv.Size = new System.Drawing.Size(1116, 484);
            this.e_dgv.TabIndex = 3;
            this.e_dgv.DoubleClick += new System.EventHandler(this.Edit_Emp);
            // 
            // emp_cont_tab
            // 
            this.emp_cont_tab.Controls.Add(this.textBox3);
            this.emp_cont_tab.Controls.Add(this.comboBox3);
            this.emp_cont_tab.Controls.Add(this.ec_add_btn);
            this.emp_cont_tab.Controls.Add(this.ec_edit_btn);
            this.emp_cont_tab.Controls.Add(this.ec_delete_btn);
            this.emp_cont_tab.Controls.Add(this.ec_fetch_btn);
            this.emp_cont_tab.Controls.Add(this.ec_dgv);
            this.emp_cont_tab.Location = new System.Drawing.Point(4, 22);
            this.emp_cont_tab.Name = "emp_cont_tab";
            this.emp_cont_tab.Size = new System.Drawing.Size(1128, 574);
            this.emp_cont_tab.TabIndex = 4;
            this.emp_cont_tab.Text = "Employer Contacts";
            this.emp_cont_tab.UseVisualStyleBackColor = true;
            this.emp_cont_tab.Enter += new System.EventHandler(this.Contact_Refresh);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(155, 31);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(161, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownWidth = 143;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Contact ID",
            "Employer",
            "Forename",
            "Surname",
            "Email",
            "Phone",
            "Mobile"});
            this.comboBox3.Location = new System.Drawing.Point(6, 31);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(143, 21);
            this.comboBox3.TabIndex = 1;
            // 
            // ec_add_btn
            // 
            this.ec_add_btn.Location = new System.Drawing.Point(885, 546);
            this.ec_add_btn.Name = "ec_add_btn";
            this.ec_add_btn.Size = new System.Drawing.Size(75, 23);
            this.ec_add_btn.TabIndex = 4;
            this.ec_add_btn.Text = "Add";
            this.ec_add_btn.UseVisualStyleBackColor = true;
            this.ec_add_btn.Click += new System.EventHandler(this.ec_add_btn_Click);
            // 
            // ec_edit_btn
            // 
            this.ec_edit_btn.Location = new System.Drawing.Point(966, 546);
            this.ec_edit_btn.Name = "ec_edit_btn";
            this.ec_edit_btn.Size = new System.Drawing.Size(75, 23);
            this.ec_edit_btn.TabIndex = 5;
            this.ec_edit_btn.Text = "View/Edit";
            this.ec_edit_btn.UseVisualStyleBackColor = true;
            this.ec_edit_btn.Click += new System.EventHandler(this.ec_edit_btn_Click);
            // 
            // ec_delete_btn
            // 
            this.ec_delete_btn.Location = new System.Drawing.Point(1047, 546);
            this.ec_delete_btn.Name = "ec_delete_btn";
            this.ec_delete_btn.Size = new System.Drawing.Size(75, 23);
            this.ec_delete_btn.TabIndex = 6;
            this.ec_delete_btn.Text = "Delete";
            this.ec_delete_btn.UseVisualStyleBackColor = true;
            this.ec_delete_btn.Click += new System.EventHandler(this.ec_delete_btn_Click);
            // 
            // ec_fetch_btn
            // 
            this.ec_fetch_btn.Location = new System.Drawing.Point(6, 3);
            this.ec_fetch_btn.Name = "ec_fetch_btn";
            this.ec_fetch_btn.Size = new System.Drawing.Size(75, 23);
            this.ec_fetch_btn.TabIndex = 0;
            this.ec_fetch_btn.Text = "Fetch Data";
            this.ec_fetch_btn.UseVisualStyleBackColor = true;
            this.ec_fetch_btn.Click += new System.EventHandler(this.ec_fetch_btn_Click);
            // 
            // ec_dgv
            // 
            this.ec_dgv.AllowUserToAddRows = false;
            this.ec_dgv.AllowUserToDeleteRows = false;
            this.ec_dgv.AllowUserToResizeRows = false;
            this.ec_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ec_dgv.Location = new System.Drawing.Point(6, 58);
            this.ec_dgv.MultiSelect = false;
            this.ec_dgv.Name = "ec_dgv";
            this.ec_dgv.ReadOnly = true;
            this.ec_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ec_dgv.Size = new System.Drawing.Size(1116, 484);
            this.ec_dgv.TabIndex = 3;
            this.ec_dgv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Edit_EmpCont);
            // 
            // Roles_tab
            // 
            this.Roles_tab.Controls.Add(this.textBox4);
            this.Roles_tab.Controls.Add(this.comboBox4);
            this.Roles_tab.Controls.Add(this.r_add_btn);
            this.Roles_tab.Controls.Add(this.r_edit_btn);
            this.Roles_tab.Controls.Add(this.r_delete_btn);
            this.Roles_tab.Controls.Add(this.r_fetch_btn);
            this.Roles_tab.Controls.Add(this.r_dgv);
            this.Roles_tab.Location = new System.Drawing.Point(4, 22);
            this.Roles_tab.Name = "Roles_tab";
            this.Roles_tab.Padding = new System.Windows.Forms.Padding(3);
            this.Roles_tab.Size = new System.Drawing.Size(1128, 574);
            this.Roles_tab.TabIndex = 1;
            this.Roles_tab.Text = "Roles";
            this.Roles_tab.UseVisualStyleBackColor = true;
            this.Roles_tab.Enter += new System.EventHandler(this.Role_Refresh);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(155, 31);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(161, 20);
            this.textBox4.TabIndex = 2;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownWidth = 143;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Roles ID",
            "Job Role",
            "Salary",
            "Employer",
            "Forename",
            "Surname",
            "Deadline",
            "Start Date",
            "End Date"});
            this.comboBox4.Location = new System.Drawing.Point(6, 31);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(143, 21);
            this.comboBox4.TabIndex = 1;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // r_add_btn
            // 
            this.r_add_btn.Location = new System.Drawing.Point(885, 546);
            this.r_add_btn.Name = "r_add_btn";
            this.r_add_btn.Size = new System.Drawing.Size(75, 23);
            this.r_add_btn.TabIndex = 4;
            this.r_add_btn.Text = "Add";
            this.r_add_btn.UseVisualStyleBackColor = true;
            this.r_add_btn.Click += new System.EventHandler(this.r_add_btn_Click_1);
            // 
            // r_edit_btn
            // 
            this.r_edit_btn.Location = new System.Drawing.Point(966, 546);
            this.r_edit_btn.Name = "r_edit_btn";
            this.r_edit_btn.Size = new System.Drawing.Size(75, 23);
            this.r_edit_btn.TabIndex = 5;
            this.r_edit_btn.Text = "View/Edit";
            this.r_edit_btn.UseVisualStyleBackColor = true;
            this.r_edit_btn.Click += new System.EventHandler(this.r_edit_btn_Click);
            // 
            // r_delete_btn
            // 
            this.r_delete_btn.Location = new System.Drawing.Point(1047, 546);
            this.r_delete_btn.Name = "r_delete_btn";
            this.r_delete_btn.Size = new System.Drawing.Size(75, 23);
            this.r_delete_btn.TabIndex = 6;
            this.r_delete_btn.Text = "Delete";
            this.r_delete_btn.UseVisualStyleBackColor = true;
            this.r_delete_btn.Click += new System.EventHandler(this.r_delete_btn_Click);
            // 
            // r_fetch_btn
            // 
            this.r_fetch_btn.Location = new System.Drawing.Point(6, 3);
            this.r_fetch_btn.Name = "r_fetch_btn";
            this.r_fetch_btn.Size = new System.Drawing.Size(75, 23);
            this.r_fetch_btn.TabIndex = 0;
            this.r_fetch_btn.Text = "Fetch Data";
            this.r_fetch_btn.UseVisualStyleBackColor = true;
            this.r_fetch_btn.Click += new System.EventHandler(this.button1_Click);
            // 
            // r_dgv
            // 
            this.r_dgv.AllowUserToAddRows = false;
            this.r_dgv.AllowUserToDeleteRows = false;
            this.r_dgv.AllowUserToResizeRows = false;
            this.r_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.r_dgv.Location = new System.Drawing.Point(6, 58);
            this.r_dgv.MultiSelect = false;
            this.r_dgv.Name = "r_dgv";
            this.r_dgv.ReadOnly = true;
            this.r_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.r_dgv.Size = new System.Drawing.Size(1116, 484);
            this.r_dgv.TabIndex = 3;
            this.r_dgv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Edit_Role);
            // 
            // vst_tab
            // 
            this.vst_tab.Controls.Add(this.vt_add_btn);
            this.vst_tab.Controls.Add(this.label2);
            this.vst_tab.Controls.Add(this.textBox5);
            this.vst_tab.Controls.Add(this.comboBox5);
            this.vst_tab.Controls.Add(this.vt_edit_btn);
            this.vst_tab.Controls.Add(this.vt_delete_btn);
            this.vst_tab.Controls.Add(this.vt_fetch_btn);
            this.vst_tab.Controls.Add(this.vt_dgv);
            this.vst_tab.Location = new System.Drawing.Point(4, 22);
            this.vst_tab.Name = "vst_tab";
            this.vst_tab.Size = new System.Drawing.Size(1128, 574);
            this.vst_tab.TabIndex = 5;
            this.vst_tab.Text = "Visiting Tutors";
            this.vst_tab.UseVisualStyleBackColor = true;
            this.vst_tab.Enter += new System.EventHandler(this.Visit_Refresh);
            // 
            // vt_add_btn
            // 
            this.vt_add_btn.Location = new System.Drawing.Point(885, 546);
            this.vt_add_btn.Name = "vt_add_btn";
            this.vt_add_btn.Size = new System.Drawing.Size(75, 23);
            this.vt_add_btn.TabIndex = 4;
            this.vt_add_btn.Text = "Add";
            this.vt_add_btn.UseVisualStyleBackColor = true;
            this.vt_add_btn.Click += new System.EventHandler(this.vt_add_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(649, 358);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 3;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(155, 31);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(161, 20);
            this.textBox5.TabIndex = 2;
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "Visiting Tutor ID",
            "Forename",
            "Surname",
            "Email"});
            this.comboBox5.Location = new System.Drawing.Point(6, 31);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(143, 21);
            this.comboBox5.TabIndex = 1;
            // 
            // vt_edit_btn
            // 
            this.vt_edit_btn.Location = new System.Drawing.Point(966, 546);
            this.vt_edit_btn.Name = "vt_edit_btn";
            this.vt_edit_btn.Size = new System.Drawing.Size(75, 23);
            this.vt_edit_btn.TabIndex = 5;
            this.vt_edit_btn.Text = "View/Edit";
            this.vt_edit_btn.UseVisualStyleBackColor = true;
            this.vt_edit_btn.Click += new System.EventHandler(this.vt_edit_btn_Click);
            // 
            // vt_delete_btn
            // 
            this.vt_delete_btn.Location = new System.Drawing.Point(1047, 546);
            this.vt_delete_btn.Name = "vt_delete_btn";
            this.vt_delete_btn.Size = new System.Drawing.Size(75, 23);
            this.vt_delete_btn.TabIndex = 6;
            this.vt_delete_btn.Text = "Delete";
            this.vt_delete_btn.UseVisualStyleBackColor = true;
            this.vt_delete_btn.Click += new System.EventHandler(this.vt_delete_btn_Click);
            // 
            // vt_fetch_btn
            // 
            this.vt_fetch_btn.Location = new System.Drawing.Point(6, 3);
            this.vt_fetch_btn.Name = "vt_fetch_btn";
            this.vt_fetch_btn.Size = new System.Drawing.Size(75, 23);
            this.vt_fetch_btn.TabIndex = 0;
            this.vt_fetch_btn.Text = "Fetch Data";
            this.vt_fetch_btn.UseVisualStyleBackColor = true;
            this.vt_fetch_btn.Click += new System.EventHandler(this.vt_fetch_btn_Click);
            // 
            // vt_dgv
            // 
            this.vt_dgv.AllowUserToAddRows = false;
            this.vt_dgv.AllowUserToDeleteRows = false;
            this.vt_dgv.AllowUserToResizeRows = false;
            this.vt_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vt_dgv.Location = new System.Drawing.Point(6, 58);
            this.vt_dgv.MultiSelect = false;
            this.vt_dgv.Name = "vt_dgv";
            this.vt_dgv.ReadOnly = true;
            this.vt_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.vt_dgv.Size = new System.Drawing.Size(1116, 484);
            this.vt_dgv.TabIndex = 3;
            this.vt_dgv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Edit_VisitingTutor);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 649);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File_Toolbar,
            this.User_toolbar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1160, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // File_Toolbar
            // 
            this.File_Toolbar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.File_Toolbar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File_Exit});
            this.File_Toolbar.Image = ((System.Drawing.Image)(resources.GetObject("File_Toolbar.Image")));
            this.File_Toolbar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.File_Toolbar.Name = "File_Toolbar";
            this.File_Toolbar.Size = new System.Drawing.Size(38, 22);
            this.File_Toolbar.Text = "File";
            this.File_Toolbar.Click += new System.EventHandler(this.toolStripDropDownButton1_Click_1);
            // 
            // File_Exit
            // 
            this.File_Exit.Name = "File_Exit";
            this.File_Exit.Size = new System.Drawing.Size(92, 22);
            this.File_Exit.Text = "Exit";
            this.File_Exit.Click += new System.EventHandler(this.File_Exit_Click);
            // 
            // User_toolbar
            // 
            this.User_toolbar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.User_toolbar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Change_user_tool,
            this.Create_New_user_tool,
            this.manageUsersToolStripMenuItem});
            this.User_toolbar.Image = ((System.Drawing.Image)(resources.GetObject("User_toolbar.Image")));
            this.User_toolbar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.User_toolbar.Name = "User_toolbar";
            this.User_toolbar.Size = new System.Drawing.Size(43, 22);
            this.User_toolbar.Text = "User";
            // 
            // Change_user_tool
            // 
            this.Change_user_tool.Name = "Change_user_tool";
            this.Change_user_tool.Size = new System.Drawing.Size(161, 22);
            this.Change_user_tool.Text = "Change User";
            this.Change_user_tool.Click += new System.EventHandler(this.Change_user_tool_Click);
            // 
            // Create_New_user_tool
            // 
            this.Create_New_user_tool.Name = "Create_New_user_tool";
            this.Create_New_user_tool.Size = new System.Drawing.Size(161, 22);
            this.Create_New_user_tool.Text = "Create New User";
            this.Create_New_user_tool.Click += new System.EventHandler(this.Create_New_user_tool_Click);
            // 
            // manageUsersToolStripMenuItem
            // 
            this.manageUsersToolStripMenuItem.Name = "manageUsersToolStripMenuItem";
            this.manageUsersToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.manageUsersToolStripMenuItem.Text = "Manage Users";
            this.manageUsersToolStripMenuItem.Click += new System.EventHandler(this.manageUsersToolStripMenuItem_Click);
            // 
            // StudentRefresh
            // 
            this.StudentRefresh.Enabled = true;
            this.StudentRefresh.Tick += new System.EventHandler(this.StudentRefresh_Tick);
            // 
            // EmployerRefresh
            // 
            this.EmployerRefresh.Enabled = true;
            this.EmployerRefresh.Tick += new System.EventHandler(this.EmployerRefresh_Tick);
            // 
            // ContactsRefresh
            // 
            this.ContactsRefresh.Enabled = true;
            this.ContactsRefresh.Tick += new System.EventHandler(this.ContactsRefresh_Tick);
            // 
            // RolesRefresh
            // 
            this.RolesRefresh.Enabled = true;
            this.RolesRefresh.Tick += new System.EventHandler(this.RolesRefresh_Tick);
            // 
            // VisitingRefresh
            // 
            this.VisitingRefresh.Enabled = true;
            this.VisitingRefresh.Tick += new System.EventHandler(this.VisitingRefresh_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1018, 637);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(130, 37);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // Asterisk_Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 680);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.s_edit_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Asterisk_Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EC Futures Dashboard";
            this.Load += new System.EventHandler(this.Asterisk_Dashboard_Load);
            this.s_edit_btn.ResumeLayout(false);
            this.dashboard_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.stud_tab.ResumeLayout(false);
            this.stud_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.s_dgv)).EndInit();
            this.emp_tab.ResumeLayout(false);
            this.emp_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.e_dgv)).EndInit();
            this.emp_cont_tab.ResumeLayout(false);
            this.emp_cont_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ec_dgv)).EndInit();
            this.Roles_tab.ResumeLayout(false);
            this.Roles_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.r_dgv)).EndInit();
            this.vst_tab.ResumeLayout(false);
            this.vst_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vt_dgv)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl s_edit_btn;
        private System.Windows.Forms.TabPage dashboard_tab;
        private System.Windows.Forms.TabPage Roles_tab;
        private System.Windows.Forms.TabPage stud_tab;
        private System.Windows.Forms.TabPage emp_tab;
        private System.Windows.Forms.TabPage emp_cont_tab;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton File_Toolbar;
        private System.Windows.Forms.ToolStripMenuItem File_Exit;
        private System.Windows.Forms.ToolStripDropDownButton User_toolbar;
        private System.Windows.Forms.ToolStripMenuItem Change_user_tool;
        private System.Windows.Forms.ToolStripMenuItem Create_New_user_tool;
        private System.Windows.Forms.ToolStripMenuItem manageUsersToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button r_fetch_btn;
        private System.Windows.Forms.DataGridView r_dgv;
        private System.Windows.Forms.TabPage vst_tab;
        private System.Windows.Forms.DataGridView s_dgv;
        private System.Windows.Forms.DataGridView e_dgv;
        private System.Windows.Forms.Button s_add_btn;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button s_delete_btn;
        private System.Windows.Forms.Button s_fetch_btn;
        private System.Windows.Forms.Button e_add_btn;
        private System.Windows.Forms.Button e_edit_btn;
        private System.Windows.Forms.Button e_delete_btn;
        private System.Windows.Forms.Button e_fetch_btn;
        private System.Windows.Forms.Button ec_add_btn;
        private System.Windows.Forms.Button ec_edit_btn;
        private System.Windows.Forms.Button ec_delete_btn;
        private System.Windows.Forms.Button ec_fetch_btn;
        private System.Windows.Forms.DataGridView ec_dgv;
        private System.Windows.Forms.Button r_add_btn;
        private System.Windows.Forms.Button r_edit_btn;
        private System.Windows.Forms.Button r_delete_btn;
        private System.Windows.Forms.Button vt_edit_btn;
        private System.Windows.Forms.Button vt_delete_btn;
        private System.Windows.Forms.Button vt_fetch_btn;
        private System.Windows.Forms.DataGridView vt_dgv;
        private System.Windows.Forms.Timer StudentRefresh;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Button vt_add_btn;
        private System.Windows.Forms.Timer EmployerRefresh;
        private System.Windows.Forms.Timer ContactsRefresh;
        private System.Windows.Forms.Timer RolesRefresh;
        private System.Windows.Forms.Timer VisitingRefresh;
    }
}