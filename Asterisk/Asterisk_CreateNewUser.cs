﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_CreateNewUser : Form
    {
        public Asterisk_CreateNewUser()
        {
            InitializeComponent();
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Create_User_btn_Click(object sender, EventArgs e)
        {
            int GroupID;
            if (Usr_Grp_bx.Text == "Standard")
            {
                if (newUser_Txt.Text == null)
                {
                    MessageBox.Show("Please enter a valid username", "EC Futures");
                }
                else
                {
                    if (newPass_Txt.Text == null)
                    {
                        MessageBox.Show("Please enter a valid password", "EC Futures");
                    }
                    else
                    {
                        GroupID = 1;
                        User_Create(GroupID);
                    }
                }
            }
            else if (Usr_Grp_bx.Text == "Administrator")
            {
                if (newUser_Txt.Text == null)
                {
                    MessageBox.Show("Please enter a valid username", "EC Futures");
                }
                else
                {
                    if (newPass_Txt.Text == null)
                    {
                        MessageBox.Show("Please enter a valid password", "EC Futures");
                    }
                    else
                    {
                        GroupID = 0;
                        User_Create(GroupID);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a user group", "EC Futures");
            }
        }

        private void Create_new_user_Load(object sender, EventArgs e)
        {
            newUser_Txt.Clear();
            newPass_Txt.Clear();
            conPass_Txt.Clear();
        }

        private void Cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void User_Create(int UID)
        {
            if (newPass_Txt.Text == conPass_Txt.Text)
            {
                DBConnect MySQLcnn = new DBConnect();
                try
                {
                    string SelectQuery = "SELECT * FROM user WHERE username = '" + newUser_Txt.Text + "'; ";
                    MySQLcnn.PerformSearch(SelectQuery);
                    if (MySQLcnn.validation == true)
                    {
                        MessageBox.Show("Username already exists", "EC Futures");
                    }
                    else
                    {
                        string password = newPass_Txt.Text;
                        byte[] encodedPassword = new UTF8Encoding().GetBytes(password);
                        byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
                        string encoded = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
                        string query = "INSERT INTO user (`u_id`, `forename`, `surname`, `username`, `password`, `type`, `active`) VALUES (NULL, '" + newForename_Txt.Text + "', '" + newSurname_Txt.Text + "' , '" + newUser_Txt.Text + "', '" + encoded + "', '" + UID + "', 1); ";
                        MySQLcnn.PerformQuery(query);
                        MessageBox.Show("User successfully created!", "EC Futures");
                        this.Hide();
                    }
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
            else
            {
                MessageBox.Show("The passwords do not match", "EC Futures");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void newForename_Txt_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
