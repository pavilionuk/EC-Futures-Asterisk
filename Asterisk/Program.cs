﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;

namespace Asterisk
{
    static class Program
    {
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Asterisk_login());
        }
    }
}
public static class userGroup
{
    public static string userGroupID;
    public static string Suspended;
    public static string a_id;
    public static int a_id2;
}

public static class DGV_Refresh
{
    public static int Student = 0;
    public static int Contact = 0;
    public static int Employer = 0;
    public static int Roles = 0;
    public static int Visit = 0;
    public static int Appointment = 0;
    public static int Roles2 = 0;
    public static int iStudent = 0;
    public static int iContact = 0;
    public static int iEmployer = 0;
    public static int iRoles = 0;
    public static int iVisit = 0;
    public static int iAppointment = 0;
    public static int iRoles2 = 0;
}

public static class EditTempStudent
{
    public static string s_id;
    public static string forename;
    public static string surname;
    public static string f_id;
    public static string d_id;
    public static string email;
    public static string mobile;
    public static string cv;
    public static string notes;
}
public static class EditTempEmployer
{
    public static string e_id;
    public static string name;
    public static string address_1;
    public static string address_2;
    public static string settlement;
    public static string county;
    public static string post_code;
    public static string country;
    public static string notes;
}
public static class EditTempContacts
{
    public static string c_id;
    public static string e_id;
    public static string forename;
    public static string surname;
    public static string email;
    public static string phone;
    public static string mobile;
    public static string notes;
}
public static class EditTempRoles
{
    public static string r_id;
    public static string title;
    public static string salary;
    public static string e_id;
    public static string c_id;
    public static string deadline;
    public static string start_date;
    public static string end_date;
    public static string notes;
}
public static class EditTempVT
{
    public static string vt_id;
    public static string forename;
    public static string surname;
    public static string email;
    public static string notes;
}
public static class EditTempApp
{
    public static string a_id;
    public static string s_id;
    public static string date;
    public static string time;
    public static string u_id;
    public static string action_points;
    public static string adviser_comments;
}
public static class EditTempReason
{
    public static int r1 = 0;
    public static int r2 = 0;
    public static int r3 = 0;
    public static int r4 = 0;
    public static int r5 = 0;
    public static int r6 = 0;
    public static int r7 = 0;
    public static int r8 = 0;
    public static int r9 = 0;
    public static int r10 = 0;
    public static int r11 = 0;
    public static int r12 = 0;

}

class DBConnect
{
    public MySqlConnection connection;
    private string server;
    private string database;
    private string uid;
    private string password;
    public Boolean validation;
    public MySqlDataAdapter mySqlDataAdapter;
    public DataSet s_DS = new DataSet();
    public DataSet e_DS = new DataSet();
    public DataSet ec_DS = new DataSet();
    public DataSet r_DS = new DataSet();
    public DataSet vt_DS = new DataSet();
    public DataSet a_DS = new DataSet();
    public DataSet rr_DS = new DataSet();
    public DataSet temp_DS = new DataSet();
    public DataTable dt1 = new DataTable();
    public DataTable dt2 = new DataTable();
    public DataTable dt3 = new DataTable();
    public DataTable dt4 = new DataTable();

    public DBConnect()
    {
        Initialize();
    }

    private void Initialize()
    {
        server = "db.atticstud.io";
        database = "ecfutures_db";
        uid = "ecfutures";
        password = "DJ_bronze";
        string connectionString;
        connectionString = "SERVER=" + server + ";" + "DATABASE=" +
        database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
        connection = new MySqlConnection(connectionString);
    }
    public void PerformQuery(string query)
    {
        connection.Open();
        MySqlCommand cmd = new MySqlCommand();
        cmd.CommandText = query;
        cmd.Connection = connection;
        cmd.ExecuteNonQuery();
        connection.Close();
    }
    public void studREF(int i)
    {
        DGV_Refresh.Student = i;
    }
    public void EmpREF(int i)
    {
        DGV_Refresh.Employer = i;
    }
    public void ContREF(int i)
    {
        DGV_Refresh.Contact = i;
    }
    public void RoleREF(int i)
    {
        DGV_Refresh.Roles = i;
    }
    public void VisiREF(int i)
    {
        DGV_Refresh.Visit = i;
    }
    public void AppsREF(int i)
    {
        DGV_Refresh.Appointment = i;
    }
    public void Roles2REF(int i)
    {
        DGV_Refresh.Roles2 = i;
    }
    public void studIND(int i)
    {
        DGV_Refresh.iStudent = i;
    }
    public void EmpIND(int i)
    {
        DGV_Refresh.iEmployer = i;
    }
    public void ContIND(int i)
    {
        DGV_Refresh.iContact = i;
    }
    public void RoleIND(int i)
    {
        DGV_Refresh.iRoles = i;
    }
    public void VisiIND(int i)
    {
        DGV_Refresh.iVisit = i;
    }
    public void AppIND(int i)
    {
        DGV_Refresh.iAppointment = i;
    }
    public void Role2IND(int i)
    {
        DGV_Refresh.iRoles2 = i;
    }
    public void tempStudStore(string sid)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = sid;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            EditTempStudent.s_id = dataReader["s_id"].ToString();
            EditTempStudent.forename = dataReader["forename"].ToString();
            EditTempStudent.surname = dataReader["surname"].ToString();
            EditTempStudent.f_id = dataReader["f_id"].ToString();
            EditTempStudent.d_id = dataReader["d_id"].ToString();
            EditTempStudent.email = dataReader["email"].ToString();
            EditTempStudent.mobile = dataReader["mobile"].ToString();
            EditTempStudent.cv = dataReader["cv"].ToString();
            EditTempStudent.notes = dataReader["notes"].ToString();
        }
        connection.Close();
    }
    public void tempEmpStore(string eid)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = eid;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            EditTempEmployer.e_id = dataReader["e_id"].ToString();
            EditTempEmployer.name = dataReader["name"].ToString();
            EditTempEmployer.address_1 = dataReader["address_1"].ToString();
            EditTempEmployer.address_2 = dataReader["address_2"].ToString();
            EditTempEmployer.settlement = dataReader["settlement"].ToString();
            EditTempEmployer.county = dataReader["county"].ToString();
            EditTempEmployer.post_code = dataReader["post_code"].ToString();
            EditTempEmployer.country = dataReader["country"].ToString();
            EditTempEmployer.notes = dataReader["notes"].ToString();
        }
        connection.Close();
    }
    public void dead(string input)
    {
        EditTempRoles.deadline = input;
    }
    public void start(string input)
    {
        EditTempRoles.start_date = input;
    }
    public void end(string input)
    {
        EditTempRoles.end_date = input;
    }
    public void date(string input)
    {
        EditTempApp.date = input;
    }
    public void tempContStore(string cid)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = cid;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            EditTempContacts.c_id = dataReader["c_id"].ToString();
            EditTempContacts.e_id = dataReader["e_id"].ToString();
            EditTempContacts.forename = dataReader["forename"].ToString();
            EditTempContacts.surname = dataReader["surname"].ToString();
            EditTempContacts.email = dataReader["email"].ToString();
            EditTempContacts.phone = dataReader["phone"].ToString();
            EditTempContacts.mobile = dataReader["mobile"].ToString();
            EditTempContacts.notes = dataReader["notes"].ToString();
        }
        connection.Close();
    }
    public void tempRoleStore(string rid)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = rid;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            EditTempRoles.r_id = dataReader["r_id"].ToString();
            EditTempRoles.title = dataReader["title"].ToString();
            EditTempRoles.salary = dataReader["salary"].ToString();
            EditTempRoles.e_id = dataReader["e_id"].ToString();
            EditTempRoles.c_id = dataReader["c_id"].ToString();
            EditTempRoles.deadline = dataReader["deadline"].ToString();
            EditTempRoles.start_date = dataReader["start_date"].ToString();
            EditTempRoles.end_date = dataReader["end_date"].ToString();
            EditTempRoles.notes = dataReader["notes"].ToString();
        }
        connection.Close();
    }
    public void tempVTStore(string vtid)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = vtid;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            EditTempVT.vt_id = dataReader["vt_id"].ToString();
            EditTempVT.forename = dataReader["forename"].ToString();
            EditTempVT.surname = dataReader["surname"].ToString();
            EditTempVT.email = dataReader["email"].ToString();
            EditTempVT.notes = dataReader["notes"].ToString();
        }
        connection.Close();
    }
    public void tempAppStore(string aid)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = aid;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            EditTempApp.a_id = dataReader["a_id"].ToString();
            EditTempApp.s_id = dataReader["s_id"].ToString();
            EditTempApp.date = dataReader["date"].ToString();
            EditTempApp.time = dataReader["time"].ToString();
            EditTempApp.u_id = dataReader["u_id"].ToString();
            EditTempApp.action_points = dataReader["action_points"].ToString();
            EditTempApp.adviser_comments = dataReader["adviser_comments"].ToString();
        }
        connection.Close();
    }
    public void a_idStore(string query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            userGroup.a_id = dataReader["a_id"].ToString();
        }
        connection.Close();
    }
    public void PerformSearch(string SelectQuery)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = SelectQuery;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            validation = true;
        }
        else
        {
            validation = false;
        }
        connection.Close();
    }
    public void r1(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r1 = 1;
        }
        else
        {
            EditTempReason.r1 = 0;
        }
        connection.Close();
    }
    public void r2(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r2 = 1;
        }
        else
        {
            EditTempReason.r2 = 0;
        }
        connection.Close();
    }
    public void r3(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r3 = 1;
        }
        else
        {
            EditTempReason.r3 = 0;
        }
        connection.Close();
    }
    public void r4(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r4 = 1;
        }
        else
        {
            EditTempReason.r4 = 0;
        }
        connection.Close();
    }
    public void r5(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r5 = 1;
        }
        else
        {
            EditTempReason.r5 = 0;
        }
        connection.Close();
    }
    public void r6(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r6 = 1;
        }
        else
        {
            EditTempReason.r6 = 0;
        }
        connection.Close();
    }
    public void r7(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r7 = 1;
        }
        else
        {
            EditTempReason.r7 = 0;
        }
        connection.Close();
    }
    public void r8(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r8 = 1;
        }
        else
        {
            EditTempReason.r8 = 0;
        }
        connection.Close();
    }
    public void r9(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r9 = 1;
        }
        else
        {
            EditTempReason.r9 = 0;
        }
        connection.Close();
    }
    public void r10(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r10 = 1;
        }
        else
        {
            EditTempReason.r10 = 0;
        }
        connection.Close();
    }
    public void r11(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r11 = 1;
        }
        else
        {
            EditTempReason.r11 = 0;
        }
        connection.Close();
    }
    public void r12(string Query)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = Query;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        if (dataReader.Read() == true)
        {
            EditTempReason.r12 = 1;
        }
        else
        {
            EditTempReason.r12 = 0;
        }
        connection.Close();
    }
    public void tempDS1(string SelectQuery)
    {
        dt1.Clear();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = SelectQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(SelectQuery, connection);
        mySqlDataAdapter.Fill(dt1);
        connection.Close();
    }
    public void tempDS2(string SelectQuery)
    {
        dt2.Clear();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = SelectQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(SelectQuery, connection);
        mySqlDataAdapter.Fill(dt2);
        connection.Close();
    }
    public void tempDS3(string SelectQuery)
    {
        dt3.Clear();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = SelectQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(SelectQuery, connection);
        mySqlDataAdapter.Fill(dt3);
        connection.Close();
    }
    public void tempDS4(string SelectQuery)
    {
        dt4.Clear();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = SelectQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(SelectQuery, connection);
        mySqlDataAdapter.Fill(dt4);
        connection.Close();
    }
    public void GetUserID(string UID)
    {
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = UID;
        scmd.Connection = connection;
        MySqlDataReader dataReader = scmd.ExecuteReader();
        while (dataReader.Read())
        {
            userGroup.userGroupID = (dataReader["type"] + "");
            userGroup.Suspended = (dataReader["active"] + "");
        }
        connection.Close();
    }
    public void backdoor()
    {
        userGroup.userGroupID = "0";
    }
    public void set_appid(int i)
    {
        userGroup.a_id2 = i;
    }
    public void FetchStudent(string MassSelectionQuery)
    {
        s_DS.Reset();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = MassSelectionQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(MassSelectionQuery, connection);
        mySqlDataAdapter.Fill(s_DS);
        connection.Close();
    }
    public void FetchEmployer(string MassSelectionQuery)
    {
        e_DS.Reset();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = MassSelectionQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(MassSelectionQuery, connection);
        mySqlDataAdapter.Fill(e_DS);
        connection.Close();
    }
    public void FetchContact(string MassSelectionQuery)
    {
        ec_DS.Reset();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = MassSelectionQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(MassSelectionQuery, connection);
        mySqlDataAdapter.Fill(ec_DS);
        connection.Close();
    }
    public void FetchRole(string MassSelectionQuery)
    {
        r_DS.Reset();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = MassSelectionQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(MassSelectionQuery, connection);
        mySqlDataAdapter.Fill(r_DS);
        connection.Close();
    }
    public void FetchVisitingTutor(string MassSelectionQuery)
    {
        vt_DS.Reset();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = MassSelectionQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(MassSelectionQuery, connection);
        mySqlDataAdapter.Fill(vt_DS);
        connection.Close();
    }
    public void FetchAppointments(string MassSelectionQuery)
    {
        a_DS.Reset();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = MassSelectionQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(MassSelectionQuery, connection);
        mySqlDataAdapter.Fill(a_DS);
        connection.Close();
    }
    public void FetchRoles2(string MassSelectionQuery)
    {
        rr_DS.Reset();
        connection.Open();
        MySqlCommand scmd = new MySqlCommand();
        scmd.CommandText = MassSelectionQuery;
        scmd.Connection = connection;
        mySqlDataAdapter = new MySqlDataAdapter(MassSelectionQuery, connection);
        mySqlDataAdapter.Fill(rr_DS);
        connection.Close();
    }
}
