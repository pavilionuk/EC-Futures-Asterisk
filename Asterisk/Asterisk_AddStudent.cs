﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_AddStudent : Form
    {
        public Asterisk_AddStudent()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void Asterisk_AddStudent_Load(object sender, EventArgs e)
        {
            try
            {
                string SelectQuery = "SELECT * FROM faculty; ";
                MySQL.tempDS1(SelectQuery);
                faculty_cmb.DisplayMember = "name";
                faculty_cmb.ValueMember = "f_id";
                faculty_cmb.DataSource = MySQL.dt1;
                SelectQuery = "SELECT * FROM degree; ";
                MySQL.tempDS2(SelectQuery);
                course_cmb.DisplayMember = "name";
                course_cmb.ValueMember = "d_id";
                course_cmb.DataSource = MySQL.dt2;
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (studentid_txt.Text == "" || forename_txt.Text == "" || surname_txt.Text == "" || faculty_cmb.Text == "" || course_cmb.Text == "" || email_txt.Text == "" || phone_txt.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string f_str = faculty_cmb.SelectedValue.ToString();
                    int f_id;
                    bool f_parsed = Int32.TryParse(f_str, out f_id);
                    string d_str = course_cmb.SelectedValue.ToString();
                    int d_id;
                    bool d_parsed = Int32.TryParse(d_str, out d_id);
                    string query = "INSERT INTO student (`s_id`, `forename`, `surname`, `f_id`, `d_id`, `email`, `mobile`, `cv`, `notes`) VALUES (" + studentid_txt.Text + ", '" + forename_txt.Text + "', '" + surname_txt.Text + "', " + f_id + ", " + d_id + ", '" + email_txt.Text + "', '" + phone_txt.Text + "', NULL, '" + notes_txt.Text + "');";
                    MySQL.PerformQuery(query);
                    MySQL.studREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
                
            }
        }
    }
}
