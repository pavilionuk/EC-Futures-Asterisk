﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_EditEmployerContacts : Form
    {
        public Asterisk_EditEmployerContacts()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void Asterisk_EditEmployerContacts_Load(object sender, EventArgs e)
        {
            try
            {
                string SelectQuery = "SELECT * FROM employer; ";
                MySQL.tempDS1(SelectQuery);
                employer_cmb.DisplayMember = "name";
                employer_cmb.ValueMember = "e_id";
                employer_cmb.DataSource = MySQL.dt1;
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
            employer_cmb.SelectedValue = EditTempContacts.e_id;
            forename_txt.Text = EditTempContacts.forename;
            surname_txt.Text = EditTempContacts.surname;
            email_txt.Text = EditTempContacts.email;
            phone_txt.Text = EditTempContacts.phone;
            mobile_txt.Text = EditTempContacts.mobile;
            notes_txt.Text = EditTempContacts.notes;
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (employer_cmb.Text == "" || forename_txt.Text == "" || surname_txt.Text == "" || email_txt.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string e_str = employer_cmb.SelectedValue.ToString();
                    int e_id;
                    bool e_parsed = Int32.TryParse(e_str, out e_id);
                    string query = "UPDATE contact SET e_id=" + e_id + ", forename='" + forename_txt.Text + "', surname='" + surname_txt.Text + "', email='" + email_txt.Text + "', phone='" + phone_txt.Text + "', mobile='" + mobile_txt.Text + "', notes='" + notes_txt.Text + "' WHERE c_id=" + EditTempContacts.c_id + ";";
                    MySQL.PerformQuery(query);
                    MySQL.ContREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }
    }
}
