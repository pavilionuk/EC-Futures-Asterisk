﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_EditAppointment : Form
    {
        public Asterisk_EditAppointment()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Asterisk_EditAppointment_Load(object sender, EventArgs e)
        {
            string dDD = EditTempApp.date.Substring(0, 2);
            string dMM = EditTempApp.date.Substring(3, 2);
            string dYYYY = EditTempApp.date.Substring(6, 4);
            try
            {
                string SelectQuery = "SELECT u_id, CONCAT(forename, ' ', surname) AS full FROM user";
                MySQL.tempDS1(SelectQuery);
                staff_cmb.DisplayMember = "full";
                staff_cmb.ValueMember = "u_id";
                staff_cmb.DataSource = MySQL.dt1;

            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
            int idYYYY;
            bool dY_parsed = Int32.TryParse(dYYYY, out idYYYY);
            int idMM;
            bool dM_parsed = Int32.TryParse(dMM, out idMM);
            int idDD;
            bool dD_parsed = Int32.TryParse(dDD, out idDD);

            date_dtp.Format = DateTimePickerFormat.Custom;
            date_dtp.CustomFormat = "dd-MM-yyyy";
            date_dtp.Value = new DateTime(idYYYY, idMM, idDD);
            action_txt.Text = EditTempApp.action_points;
            adviser_txt.Text = EditTempApp.adviser_comments;

            string query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=1";
            MySQL.r1(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=2";
            MySQL.r2(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=3";
            MySQL.r3(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=4";
            MySQL.r4(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=5";
            MySQL.r5(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=6";
            MySQL.r6(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=7";
            MySQL.r7(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=8";
            MySQL.r8(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=9";
            MySQL.r9(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=10";
            MySQL.r10(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=11";
            MySQL.r11(query);
            query = "SELECT * FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=12";
            MySQL.r12(query);

            if (EditTempReason.r1 == 1)
            {
                checkBox1.Checked = true;
            }
            if (EditTempReason.r2 == 1)
            {
                checkBox2.Checked = true;
            }
            if (EditTempReason.r3 == 1)
            {
                checkBox3.Checked = true;
            }
            if (EditTempReason.r4 == 1)
            {
                checkBox4.Checked = true;
            }
            if (EditTempReason.r5 == 1)
            {
                checkBox5.Checked = true;
            }
            if (EditTempReason.r6 == 1)
            {
                checkBox6.Checked = true;
            }
            if (EditTempReason.r7 == 1)
            {
                checkBox7.Checked = true;
            }
            if (EditTempReason.r8 == 1)
            {
                checkBox8.Checked = true;
            }
            if (EditTempReason.r9 == 1)
            {
                checkBox9.Checked = true;
            }
            if (EditTempReason.r10 == 1)
            {
                checkBox10.Checked = true;
            }
            if (EditTempReason.r11 == 1)
            {
                checkBox11.Checked = true;
            }
            if (EditTempReason.r12 == 1)
            {
                checkBox12.Checked = true;
            }
            staff_cmb.SelectedValue = EditTempApp.u_id;
            string timem = EditTempApp.time.Substring(3, 2);
            string timeh = EditTempApp.time.Substring(0, 2);
            int h, m;
            bool h_parsed = Int32.TryParse(timeh, out h);
            bool m_parsed = Int32.TryParse(timem, out m);
            hour_num.Value = h;
            min_num.Value = m;
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            try
            {
                string date = date_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                int startIndex = 0;
                int length = 10;
                string d = date.Substring(startIndex, length);
                int h = Convert.ToInt32(hour_num.Value);
                int m = Convert.ToInt32(min_num.Value);
                string time;
                if (h < 10)
                {
                    if (m < 10)
                    {
                        time = "0" + h + ":0" + m;
                    }
                    else
                    {
                        time = "0" + h + ":" + m;
                    }
                }
                else
                {
                    if (m < 10)
                    {
                        time = h + ":0" + m;
                    }
                    else
                    {
                        time = h + ":" + m;
                    }
                }
                string query = "UPDATE appointment SET a_id=" + EditTempApp.a_id + ", s_id=" + EditTempApp.s_id + ", date='" + d + "', time='" + time + "', u_id=" + staff_cmb.SelectedValue.ToString() + ", action_points='" + action_txt.Text + "', adviser_comments='" + adviser_txt.Text + "' WHERE a_id=" + EditTempApp.a_id + ";";
                MySQL.PerformQuery(query);
                if (checkBox1.Checked == true)
                {
                    if (EditTempReason.r1 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 1)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r1 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=1";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox2.Checked == true)
                {
                    if (EditTempReason.r2 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 2)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r2 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=2";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox3.Checked == true)
                {
                    if (EditTempReason.r3 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 3)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r3 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=3";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox4.Checked == true)
                {
                    if (EditTempReason.r4 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 4)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r4 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=4";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox5.Checked == true)
                {
                    if (EditTempReason.r5 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 5)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r5 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=5";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox6.Checked == true)
                {
                    if (EditTempReason.r6 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 6)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r6 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=6";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox7.Checked == true)
                {
                    if (EditTempReason.r7 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 7)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r7 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=7";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox8.Checked == true)
                {
                    if (EditTempReason.r8 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 8)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r8 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=8";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox9.Checked == true)
                {
                    if (EditTempReason.r9 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 9)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r9 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=9";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox10.Checked == true)
                {
                    if (EditTempReason.r10 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 10)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r10 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=10";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox11.Checked == true)
                {
                    if (EditTempReason.r11 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 11)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r11 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=11";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                if (checkBox12.Checked == true)
                {
                    if (EditTempReason.r12 == 0)
                    {
                        query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + EditTempApp.a_id + ", 12)";
                        MySQL.PerformQuery(query);
                    }
                }
                else
                {
                    if (EditTempReason.r12 == 1)
                    {
                        query = "DELETE FROM appointment_details WHERE a_id=" + EditTempApp.a_id + " AND ar_id=12";
                        MySQL.PerformQuery(query);
                    }
                }
                //@
                MySQL.AppsREF(1);
                this.Hide();
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
    }
}
