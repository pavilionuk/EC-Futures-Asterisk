﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_AddEmployerContacts : Form
    {
        public Asterisk_AddEmployerContacts()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void add_btn_Click(object sender, EventArgs e)
        {
            if (employer_cmb.Text == "" || forename_txt.Text == "" || surname_txt.Text == "" || email_txt.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string e_str = employer_cmb.SelectedValue.ToString();
                    int e_id;
                    bool e_parsed = Int32.TryParse(e_str, out e_id);
                    string query = "INSERT INTO contact (`c_id`, `e_id`, `forename`, `surname`, `email`, `phone`, `mobile`, `notes`) VALUES (NULL, " + e_id + ", '" + forename_txt.Text + "', '" + surname_txt.Text + "', '" + email_txt.Text + "', '" + phone_txt.Text + "', '" + mobile_txt.Text + "', '" + notes_txt.Text + "');";
                    MySQL.PerformQuery(query);
                    MySQL.ContREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void Asterisk_AddEmployerContacts_Load(object sender, EventArgs e)
        {
            try
            {
                string SelectQuery = "SELECT * FROM employer; ";
                MySQL.tempDS1(SelectQuery);
                employer_cmb.DisplayMember = "name";
                employer_cmb.ValueMember = "e_id";
                employer_cmb.DataSource = MySQL.dt1;
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
    }
}
