﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_AddVisitingTutors : Form
    {
        public Asterisk_AddVisitingTutors()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (forename_txt.Text == "" || surname_txt.Text == "" || email_txt.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string query = "INSERT INTO visiting_tutor (`vt_id`, `forename`, `surname`, `email`, `notes`) VALUES (NULL, '" + forename_txt.Text + "', '" + surname_txt.Text + "', '" + email_txt.Text + "', '" + notes_txt.Text + "');";
                    MySQL.PerformQuery(query);
                    MySQL.VisiREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }

            }
        }
    }
}
