﻿namespace Asterisk
{
    partial class Asterisk_EditRoles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asterisk_EditRoles));
            this.end_dtp = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.start_dtp = new System.Windows.Forms.DateTimePicker();
            this.deadline_dtp = new System.Windows.Forms.DateTimePicker();
            this.contact_cmb = new System.Windows.Forms.ComboBox();
            this.employer_cmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.notes_txt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.salary_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.title_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.add_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // end_dtp
            // 
            this.end_dtp.CustomFormat = "";
            this.end_dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_dtp.Location = new System.Drawing.Point(239, 64);
            this.end_dtp.MaxDate = new System.DateTime(2116, 12, 31, 0, 0, 0, 0);
            this.end_dtp.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.end_dtp.Name = "end_dtp";
            this.end_dtp.Size = new System.Drawing.Size(209, 20);
            this.end_dtp.TabIndex = 87;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(236, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 99;
            this.label7.Text = "End date";
            // 
            // start_dtp
            // 
            this.start_dtp.CustomFormat = "";
            this.start_dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.start_dtp.Location = new System.Drawing.Point(239, 25);
            this.start_dtp.MaxDate = new System.DateTime(2116, 12, 31, 0, 0, 0, 0);
            this.start_dtp.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.start_dtp.Name = "start_dtp";
            this.start_dtp.Size = new System.Drawing.Size(209, 20);
            this.start_dtp.TabIndex = 86;
            // 
            // deadline_dtp
            // 
            this.deadline_dtp.CustomFormat = "";
            this.deadline_dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.deadline_dtp.Location = new System.Drawing.Point(15, 183);
            this.deadline_dtp.MaxDate = new System.DateTime(2116, 12, 31, 0, 0, 0, 0);
            this.deadline_dtp.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.deadline_dtp.Name = "deadline_dtp";
            this.deadline_dtp.Size = new System.Drawing.Size(209, 20);
            this.deadline_dtp.TabIndex = 85;
            // 
            // contact_cmb
            // 
            this.contact_cmb.FormattingEnabled = true;
            this.contact_cmb.Location = new System.Drawing.Point(15, 143);
            this.contact_cmb.Name = "contact_cmb";
            this.contact_cmb.Size = new System.Drawing.Size(209, 21);
            this.contact_cmb.TabIndex = 84;
            // 
            // employer_cmb
            // 
            this.employer_cmb.FormattingEnabled = true;
            this.employer_cmb.Location = new System.Drawing.Point(15, 103);
            this.employer_cmb.Name = "employer_cmb";
            this.employer_cmb.Size = new System.Drawing.Size(209, 21);
            this.employer_cmb.TabIndex = 83;
            this.employer_cmb.SelectedIndexChanged += new System.EventHandler(this.employer_cmb_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(236, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 98;
            this.label1.Text = "Notes";
            // 
            // notes_txt
            // 
            this.notes_txt.Location = new System.Drawing.Point(239, 103);
            this.notes_txt.Multiline = true;
            this.notes_txt.Name = "notes_txt";
            this.notes_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.notes_txt.Size = new System.Drawing.Size(209, 100);
            this.notes_txt.TabIndex = 88;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(236, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 97;
            this.label9.Text = "Start date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 166);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 96;
            this.label8.Text = "Deadline";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 95;
            this.label6.Text = "Contact Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 94;
            this.label5.Text = "Employer*";
            // 
            // salary_txt
            // 
            this.salary_txt.Location = new System.Drawing.Point(15, 64);
            this.salary_txt.Name = "salary_txt";
            this.salary_txt.Size = new System.Drawing.Size(209, 20);
            this.salary_txt.TabIndex = 82;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 93;
            this.label4.Text = "Salary*";
            // 
            // title_txt
            // 
            this.title_txt.Location = new System.Drawing.Point(15, 25);
            this.title_txt.Name = "title_txt";
            this.title_txt.Size = new System.Drawing.Size(209, 20);
            this.title_txt.TabIndex = 81;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 92;
            this.label3.Text = "Job Role*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 91;
            this.label2.Text = "* is a required field";
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(373, 209);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(75, 23);
            this.add_btn.TabIndex = 89;
            this.add_btn.Text = "Update";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(292, 209);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(75, 23);
            this.cancel_btn.TabIndex = 90;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // Asterisk_EditRoles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 241);
            this.Controls.Add(this.end_dtp);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.start_dtp);
            this.Controls.Add(this.deadline_dtp);
            this.Controls.Add(this.contact_cmb);
            this.Controls.Add(this.employer_cmb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.notes_txt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.salary_txt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.title_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.cancel_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Asterisk_EditRoles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Role";
            this.Load += new System.EventHandler(this.Asterisk_EditRoles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker end_dtp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker start_dtp;
        private System.Windows.Forms.DateTimePicker deadline_dtp;
        private System.Windows.Forms.ComboBox contact_cmb;
        private System.Windows.Forms.ComboBox employer_cmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox notes_txt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox salary_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox title_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.Button cancel_btn;
    }
}