﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_AddAppointment : Form
    {
        public Asterisk_AddAppointment()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Asterisk_AddAppointment_Load(object sender, EventArgs e)
        {
            try
            {
                string SelectQuery = "SELECT u_id, CONCAT(forename, ' ', surname) AS full FROM user";
                MySQL.tempDS4(SelectQuery);
                staff_cmb.DisplayMember = "full";
                staff_cmb.ValueMember = "u_id";
                staff_cmb.DataSource = MySQL.dt4;
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            try
            {
                string query = "SELECT a_id FROM appointment ORDER BY a_id DESC LIMIT 1";
                MySQL.a_idStore(query);
                int a_id;
                bool f_parsed = Int32.TryParse(userGroup.a_id, out a_id);
                a_id++;
                MySQL.set_appid(a_id);
                string date = date_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                int startIndex = 0;
                int length = 10;
                string d = date.Substring(startIndex, length);
                int h = Convert.ToInt32(hour_num.Value);
                int m = Convert.ToInt32(min_num.Value);
                string time;
                if (h < 10)
                {
                    if (m < 10)
                    {
                        time = "0" + h + ":0" + m;
                    }
                    else
                    {
                        time = "0" + h + ":" + m;
                    }
                }
                else
                {
                    if (m < 10)
                    {
                        time = h + ":0" + m;
                    }
                    else
                    {
                        time = h + ":" + m;
                    }
                }
                query = "INSERT INTO appointment (`a_id`, `s_id`, `date`, `time`, `u_id`, `action_points`, `adviser_comments`) VALUES (" + a_id + ", " + EditTempStudent.s_id + ", '" + d + "', '" + time + "', '" + staff_cmb.SelectedValue.ToString() + "', '" + action_txt.Text + "', '" + adviser_txt.Text + "')";
                MySQL.PerformQuery(query);
                if (checkBox1.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 1 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox2.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 2 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox3.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 3 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox4.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 4 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox5.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 5 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox6.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 6 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox7.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 7 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox8.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 8 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox9.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 9 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox10.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 10 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox11.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 11 + ")";
                    MySQL.PerformQuery(query);
                }
                if (checkBox12.Checked == true)
                {
                    query = "INSERT INTO appointment_details (`a_id`, `ar_id`) VALUES (" + a_id + ", " + 12 + ")";
                    MySQL.PerformQuery(query);
                }
                MySQL.AppsREF(1);
                this.Hide();
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
    }
}
