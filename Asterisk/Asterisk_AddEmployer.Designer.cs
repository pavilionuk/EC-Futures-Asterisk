﻿namespace Asterisk
{
    partial class Asterisk_AddEmployer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asterisk_AddEmployer));
            this.notes_txt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.postcode_txt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.county_txt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.address1_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.employer_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.add_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.address2_txt = new System.Windows.Forms.TextBox();
            this.settlement_txt = new System.Windows.Forms.TextBox();
            this.country_cmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // notes_txt
            // 
            this.notes_txt.Location = new System.Drawing.Point(239, 104);
            this.notes_txt.Multiline = true;
            this.notes_txt.Name = "notes_txt";
            this.notes_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.notes_txt.Size = new System.Drawing.Size(209, 99);
            this.notes_txt.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(236, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 48;
            this.label9.Text = "Country*";
            // 
            // postcode_txt
            // 
            this.postcode_txt.Location = new System.Drawing.Point(239, 25);
            this.postcode_txt.Name = "postcode_txt";
            this.postcode_txt.Size = new System.Drawing.Size(209, 20);
            this.postcode_txt.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(236, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "Post/Zip code*";
            // 
            // county_txt
            // 
            this.county_txt.Location = new System.Drawing.Point(15, 183);
            this.county_txt.Name = "county_txt";
            this.county_txt.Size = new System.Drawing.Size(209, 20);
            this.county_txt.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "County*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Settlement (Town/City)*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Address 2";
            // 
            // address1_txt
            // 
            this.address1_txt.Location = new System.Drawing.Point(15, 64);
            this.address1_txt.Name = "address1_txt";
            this.address1_txt.Size = new System.Drawing.Size(209, 20);
            this.address1_txt.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Address 1*";
            // 
            // employer_txt
            // 
            this.employer_txt.Location = new System.Drawing.Point(15, 25);
            this.employer_txt.Name = "employer_txt";
            this.employer_txt.Size = new System.Drawing.Size(209, 20);
            this.employer_txt.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "Employer*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "* is a required field";
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(373, 209);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(75, 23);
            this.add_btn.TabIndex = 8;
            this.add_btn.Text = "Add";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(292, 209);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(75, 23);
            this.cancel_btn.TabIndex = 9;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // address2_txt
            // 
            this.address2_txt.Location = new System.Drawing.Point(15, 104);
            this.address2_txt.Name = "address2_txt";
            this.address2_txt.Size = new System.Drawing.Size(209, 20);
            this.address2_txt.TabIndex = 2;
            // 
            // settlement_txt
            // 
            this.settlement_txt.Location = new System.Drawing.Point(15, 143);
            this.settlement_txt.Name = "settlement_txt";
            this.settlement_txt.Size = new System.Drawing.Size(209, 20);
            this.settlement_txt.TabIndex = 3;
            // 
            // country_cmb
            // 
            this.country_cmb.FormattingEnabled = true;
            this.country_cmb.Items.AddRange(new object[] {
            "Afghanistan",
            "Albania",
            "Algeria",
            "American Samoa",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Australia",
            "Austria",
            "Azerbajan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegovina",
            "Botswana",
            "Brazil",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Chile",
            "China",
            "Colombia",
            "Costa Rica",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Democratic Republic Congo",
            "Denmark",
            "Djibouti",
            "Dominican Republic",
            "East Timor",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "England",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Faroe Islands",
            "Fiji",
            "Finland",
            "France",
            "French Polynesia",
            "Gambia",
            "Georgia (Sakartvelo)",
            "Germany",
            "Gabon",
            "Ghana",
            "Greece",
            "Greenland - Kalaallit Nunaat",
            "Grenada",
            "Gouadeloupe",
            "Guam",
            "Guatemala",
            "Guernsey",
            "Guyana",
            "Guyane",
            "Haiti",
            "Honduras",
            "Hong Kong",
            "Hrvatska (Croatia)",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Ireland",
            "Israel",
            "Italy",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Korea Republic",
            "Kosovo",
            "Kurdistan",
            "Kuwait",
            "Kyrgyzstan",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libyan Arab Jamahiriya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macau",
            "Macedonia",
            "Malawi",
            "Malaysia",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Mauritania",
            "Martinique",
            "Mauritius",
            "Mexico",
            "Micronesia",
            "Moldova",
            "Monaco",
            "Mongolia",
            "Morocco",
            "Mozambique",
            "Namibia",
            "Nepal",
            "Netherlands",
            "Netherlands Antilles",
            "New Caledonia",
            "New Zealand (Aotearoa)",
            "Nicaragua",
            "Nigeria",
            "Niue",
            "Norfolk Island",
            "Northern Ireland",
            "Northern Mariana Islands",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Palestina",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation (AsianPart)",
            "Russian Federation (European Part)",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Vincent and the Grenadines",
            "Samoa (American Samoa)",
            "Samoa (Western Samoa)",
            "San Marino",
            "Saudi Arabia",
            "Scotland",
            "Senegal",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "Somaliland",
            "South Africa",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Svalbard and Jan Mayen",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syrian Arab Republic",
            "Taiwan",
            "Tanzania",
            "Thailand",
            "Tibet",
            "Togo",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks and Caicos Islands",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United Kingdom",
            "Uruguay",
            "USA",
            "Uzbekistan",
            "Vatican City State - Holy See",
            "Venezuela",
            "Viet Nam",
            "Virgin Islands (British)",
            "Virgin Islands (U.S.)",
            "Wales",
            "Yemen",
            "Yugoslavia",
            "Zambia",
            "Zimbabwe"});
            this.country_cmb.Location = new System.Drawing.Point(239, 64);
            this.country_cmb.Name = "country_cmb";
            this.country_cmb.Size = new System.Drawing.Size(209, 21);
            this.country_cmb.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(236, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Notes";
            // 
            // Asterisk_AddEmployer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 241);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.country_cmb);
            this.Controls.Add(this.settlement_txt);
            this.Controls.Add(this.address2_txt);
            this.Controls.Add(this.notes_txt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.postcode_txt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.county_txt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.address1_txt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.employer_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.cancel_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Asterisk_AddEmployer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Employer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox notes_txt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox postcode_txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox county_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox address1_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox employer_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.TextBox address2_txt;
        private System.Windows.Forms.TextBox settlement_txt;
        private System.Windows.Forms.ComboBox country_cmb;
        private System.Windows.Forms.Label label1;
    }
}