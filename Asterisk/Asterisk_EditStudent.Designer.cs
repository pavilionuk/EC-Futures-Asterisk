﻿namespace Asterisk
{
    partial class Asterisk_EditStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asterisk_EditStudent));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.notes_txt = new System.Windows.Forms.TextBox();
            this.cv_stat_txt = new System.Windows.Forms.TextBox();
            this.view_btn = new System.Windows.Forms.Button();
            this.upload_btn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.phone_txt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.email_txt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.course_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.faculty_cmb = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.surname_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.forename_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.studentid_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.add_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.a_add_btn = new System.Windows.Forms.Button();
            this.a_edit_btn = new System.Windows.Forms.Button();
            this.a_del_btn = new System.Windows.Forms.Button();
            this.a_dgv = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pt_open_btn = new System.Windows.Forms.Button();
            this.r_add_btn = new System.Windows.Forms.Button();
            this.r_edit_btn = new System.Windows.Forms.Button();
            this.r_del_btn = new System.Windows.Forms.Button();
            this.r_dgv = new System.Windows.Forms.DataGridView();
            this.Appointment_ref = new System.Windows.Forms.Timer(this.components);
            this.Roles_ref = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.a_dgv)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.r_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(618, 356);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.notes_txt);
            this.tabPage1.Controls.Add(this.cv_stat_txt);
            this.tabPage1.Controls.Add(this.view_btn);
            this.tabPage1.Controls.Add(this.upload_btn);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.phone_txt);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.email_txt);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.course_cmb);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.faculty_cmb);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.surname_txt);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.forename_txt);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.studentid_txt);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.add_btn);
            this.tabPage1.Controls.Add(this.cancel_btn);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(610, 330);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Edit Student";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(187, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(253, 58);
            this.pictureBox1.TabIndex = 77;
            this.pictureBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(298, 184);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 76;
            this.label10.Text = "Notes";
            // 
            // notes_txt
            // 
            this.notes_txt.Location = new System.Drawing.Point(298, 200);
            this.notes_txt.Multiline = true;
            this.notes_txt.Name = "notes_txt";
            this.notes_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.notes_txt.Size = new System.Drawing.Size(304, 100);
            this.notes_txt.TabIndex = 75;
            // 
            // cv_stat_txt
            // 
            this.cv_stat_txt.Location = new System.Drawing.Point(298, 161);
            this.cv_stat_txt.Name = "cv_stat_txt";
            this.cv_stat_txt.ReadOnly = true;
            this.cv_stat_txt.Size = new System.Drawing.Size(142, 20);
            this.cv_stat_txt.TabIndex = 74;
            this.cv_stat_txt.Text = "No CV";
            // 
            // view_btn
            // 
            this.view_btn.Location = new System.Drawing.Point(446, 162);
            this.view_btn.Name = "view_btn";
            this.view_btn.Size = new System.Drawing.Size(75, 20);
            this.view_btn.TabIndex = 73;
            this.view_btn.Text = "View";
            this.view_btn.UseVisualStyleBackColor = true;
            // 
            // upload_btn
            // 
            this.upload_btn.Location = new System.Drawing.Point(527, 162);
            this.upload_btn.Name = "upload_btn";
            this.upload_btn.Size = new System.Drawing.Size(75, 20);
            this.upload_btn.TabIndex = 72;
            this.upload_btn.Text = "Upload";
            this.upload_btn.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(298, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 71;
            this.label9.Text = "CV";
            // 
            // phone_txt
            // 
            this.phone_txt.Location = new System.Drawing.Point(298, 122);
            this.phone_txt.Name = "phone_txt";
            this.phone_txt.Size = new System.Drawing.Size(304, 20);
            this.phone_txt.TabIndex = 70;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(295, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 69;
            this.label8.Text = "Phone*";
            // 
            // email_txt
            // 
            this.email_txt.Location = new System.Drawing.Point(7, 280);
            this.email_txt.Name = "email_txt";
            this.email_txt.Size = new System.Drawing.Size(285, 20);
            this.email_txt.TabIndex = 68;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "Email*";
            // 
            // course_cmb
            // 
            this.course_cmb.FormattingEnabled = true;
            this.course_cmb.Location = new System.Drawing.Point(7, 240);
            this.course_cmb.Name = "course_cmb";
            this.course_cmb.Size = new System.Drawing.Size(285, 21);
            this.course_cmb.TabIndex = 66;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 224);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 65;
            this.label6.Text = "Degree*";
            // 
            // faculty_cmb
            // 
            this.faculty_cmb.FormattingEnabled = true;
            this.faculty_cmb.Location = new System.Drawing.Point(7, 200);
            this.faculty_cmb.Name = "faculty_cmb";
            this.faculty_cmb.Size = new System.Drawing.Size(285, 21);
            this.faculty_cmb.TabIndex = 64;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 63;
            this.label5.Text = "Faculty*";
            // 
            // surname_txt
            // 
            this.surname_txt.Location = new System.Drawing.Point(156, 162);
            this.surname_txt.Name = "surname_txt";
            this.surname_txt.Size = new System.Drawing.Size(136, 20);
            this.surname_txt.TabIndex = 62;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(153, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 61;
            this.label4.Text = "Surname*";
            // 
            // forename_txt
            // 
            this.forename_txt.Location = new System.Drawing.Point(7, 161);
            this.forename_txt.Name = "forename_txt";
            this.forename_txt.Size = new System.Drawing.Size(132, 20);
            this.forename_txt.TabIndex = 60;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "Forename*";
            // 
            // studentid_txt
            // 
            this.studentid_txt.Location = new System.Drawing.Point(7, 122);
            this.studentid_txt.Name = "studentid_txt";
            this.studentid_txt.Size = new System.Drawing.Size(285, 20);
            this.studentid_txt.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 313);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "* is a required field";
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(527, 306);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(75, 23);
            this.add_btn.TabIndex = 56;
            this.add_btn.Text = "Update";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click_1);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(446, 306);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(75, 23);
            this.cancel_btn.TabIndex = 55;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 54;
            this.label1.Text = "Student ID*";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.a_add_btn);
            this.tabPage2.Controls.Add(this.a_edit_btn);
            this.tabPage2.Controls.Add(this.a_del_btn);
            this.tabPage2.Controls.Add(this.a_dgv);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(610, 330);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Appointments";
            // 
            // a_add_btn
            // 
            this.a_add_btn.Location = new System.Drawing.Point(365, 300);
            this.a_add_btn.Name = "a_add_btn";
            this.a_add_btn.Size = new System.Drawing.Size(75, 23);
            this.a_add_btn.TabIndex = 3;
            this.a_add_btn.Text = "Add";
            this.a_add_btn.UseVisualStyleBackColor = true;
            this.a_add_btn.Click += new System.EventHandler(this.a_add_btn_Click);
            // 
            // a_edit_btn
            // 
            this.a_edit_btn.Location = new System.Drawing.Point(446, 300);
            this.a_edit_btn.Name = "a_edit_btn";
            this.a_edit_btn.Size = new System.Drawing.Size(75, 23);
            this.a_edit_btn.TabIndex = 2;
            this.a_edit_btn.Text = "View/Edit";
            this.a_edit_btn.UseVisualStyleBackColor = true;
            this.a_edit_btn.Click += new System.EventHandler(this.a_edit_btn_Click);
            // 
            // a_del_btn
            // 
            this.a_del_btn.Location = new System.Drawing.Point(527, 300);
            this.a_del_btn.Name = "a_del_btn";
            this.a_del_btn.Size = new System.Drawing.Size(75, 23);
            this.a_del_btn.TabIndex = 1;
            this.a_del_btn.Text = "Delete";
            this.a_del_btn.UseVisualStyleBackColor = true;
            this.a_del_btn.Click += new System.EventHandler(this.a_del_btn_Click);
            // 
            // a_dgv
            // 
            this.a_dgv.AllowUserToAddRows = false;
            this.a_dgv.AllowUserToDeleteRows = false;
            this.a_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.a_dgv.Location = new System.Drawing.Point(6, 6);
            this.a_dgv.MultiSelect = false;
            this.a_dgv.Name = "a_dgv";
            this.a_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.a_dgv.Size = new System.Drawing.Size(596, 288);
            this.a_dgv.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.pt_open_btn);
            this.tabPage3.Controls.Add(this.r_add_btn);
            this.tabPage3.Controls.Add(this.r_edit_btn);
            this.tabPage3.Controls.Add(this.r_del_btn);
            this.tabPage3.Controls.Add(this.r_dgv);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(610, 330);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Roles";
            // 
            // pt_open_btn
            // 
            this.pt_open_btn.Location = new System.Drawing.Point(6, 300);
            this.pt_open_btn.Name = "pt_open_btn";
            this.pt_open_btn.Size = new System.Drawing.Size(110, 23);
            this.pt_open_btn.TabIndex = 8;
            this.pt_open_btn.Text = "Placement Tracking";
            this.pt_open_btn.UseVisualStyleBackColor = true;
            // 
            // r_add_btn
            // 
            this.r_add_btn.Location = new System.Drawing.Point(365, 300);
            this.r_add_btn.Name = "r_add_btn";
            this.r_add_btn.Size = new System.Drawing.Size(75, 23);
            this.r_add_btn.TabIndex = 7;
            this.r_add_btn.Text = "Add";
            this.r_add_btn.UseVisualStyleBackColor = true;
            // 
            // r_edit_btn
            // 
            this.r_edit_btn.Location = new System.Drawing.Point(446, 300);
            this.r_edit_btn.Name = "r_edit_btn";
            this.r_edit_btn.Size = new System.Drawing.Size(75, 23);
            this.r_edit_btn.TabIndex = 6;
            this.r_edit_btn.Text = "View/Edit";
            this.r_edit_btn.UseVisualStyleBackColor = true;
            // 
            // r_del_btn
            // 
            this.r_del_btn.Location = new System.Drawing.Point(527, 300);
            this.r_del_btn.Name = "r_del_btn";
            this.r_del_btn.Size = new System.Drawing.Size(75, 23);
            this.r_del_btn.TabIndex = 5;
            this.r_del_btn.Text = "Delete";
            this.r_del_btn.UseVisualStyleBackColor = true;
            this.r_del_btn.Click += new System.EventHandler(this.r_del_btn_Click);
            // 
            // r_dgv
            // 
            this.r_dgv.AllowUserToAddRows = false;
            this.r_dgv.AllowUserToDeleteRows = false;
            this.r_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.r_dgv.Location = new System.Drawing.Point(6, 6);
            this.r_dgv.MultiSelect = false;
            this.r_dgv.Name = "r_dgv";
            this.r_dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.r_dgv.Size = new System.Drawing.Size(596, 288);
            this.r_dgv.TabIndex = 4;
            // 
            // Appointment_ref
            // 
            this.Appointment_ref.Enabled = true;
            this.Appointment_ref.Tick += new System.EventHandler(this.Appointment_ref_Tick);
            // 
            // Roles_ref
            // 
            this.Roles_ref.Enabled = true;
            this.Roles_ref.Tick += new System.EventHandler(this.Roles_ref_Tick);
            // 
            // Asterisk_EditStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(618, 360);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Asterisk_EditStudent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Student";
            this.Load += new System.EventHandler(this.Asterisk_EditStudent_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.a_dgv)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.r_dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox notes_txt;
        private System.Windows.Forms.TextBox cv_stat_txt;
        private System.Windows.Forms.Button view_btn;
        private System.Windows.Forms.Button upload_btn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox phone_txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox email_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox course_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox faculty_cmb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox surname_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox forename_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox studentid_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button a_add_btn;
        private System.Windows.Forms.Button a_edit_btn;
        private System.Windows.Forms.Button a_del_btn;
        private System.Windows.Forms.DataGridView a_dgv;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button pt_open_btn;
        private System.Windows.Forms.Button r_add_btn;
        private System.Windows.Forms.Button r_edit_btn;
        private System.Windows.Forms.Button r_del_btn;
        private System.Windows.Forms.DataGridView r_dgv;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer Appointment_ref;
        private System.Windows.Forms.Timer Roles_ref;
    }
}